import React, { useState } from 'react'
import { Box } from '@mui/material'
import SignIn from './components/SignIn'
import HomePage from './components/HomePage'
import { Routes, Route, Navigate } from 'react-router-dom'
import AdminPage from './components/AdminPage'
export interface User {
  login?: string
  isAuth?: boolean
  isAdmin?: boolean
}

function App() {
  const [user, setUser] = React.useState<User>({})

  function ProtectedAuth({ children }: { children: JSX.Element }) {
    return user.isAuth ? children : <Navigate to='/login' />
  }

  function ProtectedAdmin({ children }: { children: JSX.Element }) {
    return user.isAdmin ? children : <Navigate to='/login' />
  }

  return (
    <Box className='App'>
      <Routes>
        <Route path='/'>
          <Route
            index
            element={
              <ProtectedAuth>
                <HomePage />
              </ProtectedAuth>
            }
          />
          <Route path='login' element={<SignIn user={user} setUser={setUser} />} />
          <Route
            path='admin'
            element={
              <ProtectedAdmin>
                <AdminPage />
              </ProtectedAdmin>
            }
          />
        </Route>
      </Routes>
    </Box>
  )
}

export default App
