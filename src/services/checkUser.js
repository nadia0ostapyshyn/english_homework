import { userInfo } from '../userInfo'

const checkUser = (login, password) => {
  const authorisedUser = userInfo.find((user) => {
    return user.login === login && user.password === password
  })

  const doesExist = Boolean(authorisedUser)

  const isAdmin = authorisedUser?.isAdmin === true

  return { doesExist: doesExist, isAdmin: isAdmin }
}

export { checkUser }
