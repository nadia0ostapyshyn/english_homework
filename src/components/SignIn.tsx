import * as React from 'react'
import Button from '@mui/material/Button'
import CssBaseline from '@mui/material/CssBaseline'
import TextField from '@mui/material/TextField'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Container from '@mui/material/Container'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import { checkUser } from '../services/checkUser'
import { useNavigate } from 'react-router-dom'
import { useState } from 'react'
import { User } from '../App'

const theme = createTheme()

interface SignInProps {
  user: User
  setUser: React.Dispatch<React.SetStateAction<User>>
}

const SignIn = ({ user, setUser }: SignInProps) => {
  const navigate = useNavigate()

  const [isCorrectCredentials, setIsCorrectCredentials] = useState(true)

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    const data = new FormData(event.currentTarget)
    const login = (data.get('login') as string) || null
    const password = data.get('password') as string

    const { doesExist, isAdmin } = checkUser(login, password)

    setIsCorrectCredentials(doesExist)

    setUser({
      login: login ?? undefined,
      isAuth: doesExist,
      isAdmin: isAdmin,
    })
    
    if (isAdmin) {
      navigate('/admin')
    } else if (doesExist) {
      navigate('/')
    }
  }

  return (
    <ThemeProvider theme={theme}>
      <Container component='main' maxWidth='xs'>
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Typography component='h1' variant='h5'>
            Sign in
          </Typography>
          <Box component='form' onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin='normal'
              required
              fullWidth
              id='login'
              label='Login'
              name='login'
              autoComplete='login'
              autoFocus
              error={!isCorrectCredentials}
            />
            <TextField
              margin='normal'
              required
              fullWidth
              name='password'
              label='Password'
              type='password'
              id='password'
              autoComplete='current-password'
              error={!isCorrectCredentials}
            />
            <FormControlLabel
              control={<Checkbox value='remember' color='primary' />}
              label='Remember me'
            />
            <Button type='submit' fullWidth variant='contained' sx={{ mt: 3, mb: 2 }}>
              Sign In
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  )
}

export default SignIn
